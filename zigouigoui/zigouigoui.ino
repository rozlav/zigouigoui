#include <FastLED.h>

// RPM and light related variables
volatile byte half_revolutions;
unsigned int rpm;
unsigned long timeold;
float maxlightRPM; // at what rpm is the light full on
float targetLight; // target light intensity (0-1)
float lightIntensity; // actual current light intensity

// LED strip parameters
#define LED_PIN 4
#define NUM_LEDS    36
#define CHIPSET     WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

// pinout
#define BUZZER_PIN 3
#define LABY_PIN 5
#define FINISH_PIN 6
 
 void setup()
 {
    Serial.begin(115200);
    attachInterrupt(0, magnet_detect, RISING);//Initialize the intterrupt pin (Arduino digital pin 2)
    half_revolutions = 0;
    rpm = 0;
    timeold = 0;
    maxlightRPM = 100;
    targetLight = 0;
    lightIntensity = 0;

    FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalSMD5050 );
    FastLED.setBrightness( 200 );

    pinMode(BUZZER_PIN, OUTPUT);
    pinMode(LABY_PIN, INPUT_PULLUP);
    pinMode(FINISH_PIN, INPUT_PULLUP);
 }
 
 void loop()
 {
    // measure RPM 
    if (half_revolutions >= 1) { 
      rpm = 30*1000/(millis() - timeold)*half_revolutions;
      timeold = millis();
      half_revolutions = 0;
      targetLight = constrain(float(rpm),0.0,maxlightRPM)/maxlightRPM;
    }

    // switch off light when wheel doesnt turn
    if( (millis() - timeold) >= 2000 ){
      targetLight = 0;
    }

    // scale light intensity smoothly, according to RPM
    float t = 0.02;
    lightIntensity = (lightIntensity + targetLight * t) / (1+t);
    Serial.print(" lightIntensity : ");
    Serial.println(lightIntensity);
    fill_solid(leds, NUM_LEDS, CRGB( lightIntensity*255,lightIntensity*120,lightIntensity*30) );
    FastLED.show();

    // laby touch - detect when "loose" - only when wheel is turning
    if(targetLight > 0.2){
      if (digitalRead(LABY_PIN)==LOW) {
        tone(BUZZER_PIN, 120);
        fill_solid(leds, NUM_LEDS, CRGB( 255,50,0 ));
        FastLED.show();  
        delay(300);
        noTone(BUZZER_PIN); 
      }

      // detect when game is finished
      if (digitalRead(FINISH_PIN)==LOW) {
        fill_solid(leds, NUM_LEDS, CRGB( 60,255,0 ));
        FastLED.show();
        delay(5000);
      }
    }

    delay(10);  
}

 void magnet_detect()//This function is called whenever a magnet/interrupt is detected by the arduino
 {
   half_revolutions++;
   Serial.println("detect");
 }
