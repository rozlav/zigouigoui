/* Arduino tutorial2- Buzzer / Piezo Speaker
   More info and circuit: http://www.ardumotive.com/how-to-use-a-buzzer-en.html
   Dev: Michalis Vasilakis // Date: 9/6/2015 // www.ardumotive.com */

const int buzzer = 11; 
int lait2 =  A1;      
int touche =  A0;      

void setup(){
  pinMode(lait2, OUTPUT);
  pinMode(touche, INPUT_PULLUP);
}

void loop(){
  // if hall
  if (digitalRead(touche)==LOW) {
    digitalWrite(lait2, HIGH); 
    tone(buzzer, 440);      
    delay(200); 
    tone(buzzer, 740);      
    delay(100);  
    tone(buzzer, 340);      
    delay(100);  
    tone(buzzer, 300);      
    delay(100);  
    tone(buzzer, 220);      
    delay(200);  
    tone(buzzer, 140);      
    delay(200);
    tone(buzzer, 80);      
    delay(400);         
  }
  else {
    noTone(buzzer);     // Stop sound...
    digitalWrite(lait2, LOW);   
  }
  // fin if hall
}
